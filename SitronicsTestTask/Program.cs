﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;


namespace SitronicsTestTask
{
    internal sealed class Program
    {
        public static void Main(string[] args)
        {
            var filepath = "data.json";
            var isCreatingData = false;
            var isToItterate = false;


            Console.WriteLine("-f filepath - указать путь до файла данных");
            Console.WriteLine("без аргументов - будет подгружен файл data.json");
            Console.WriteLine("-w filepath - создать данные по пути filepath");
            Console.WriteLine("-i - проитерироваться по маршруту поезда");
            var input = args.Length > 0 ? args : Console.ReadLine()?.Trim().Split();

            if (input is null)
                return;
            for (var i = 0; i < input.Length; i++)
            {
                var var = input[i];
                switch (var)
                {
                    case "-f" when i + 1 < input.Length && input[i + 1].Length > 0:
                        filepath = input[i + 1];
                        break;
                    case "-w" when input[i + 1].Length > 0:
                        filepath = input[i + 1];
                        isCreatingData = true;
                        break;
                    case "-i":
                        isToItterate = true;
                        break;
                }
            }

            InputParser inputParser = new InputParser(filepath, isCreatingData, isToItterate);

            inputParser.HandleInput();
        }
    }
}