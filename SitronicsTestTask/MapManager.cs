﻿using System;

namespace SitronicsTestTask;

public class MapManager
{
    private Map _map;
    private bool _isCrashed = false;
    private int _activePathes = 0;
    private bool _isToIttarate = false;

    public MapManager(Map map)
    {
        _map = map;
        _activePathes = map.Trains.Count;
    }

    private void OnPathEnded(Train train)
    {
        _activePathes--;
        _map.Trains.Remove(train);
    }

    public void Init(bool isToItterate)
    {
        _isToIttarate = isToItterate;
        _map.TrainHandlers.ForEach(handler => handler.OnTrainCrash += OnTrainCrash);
        _map.Trains.ForEach(train =>
        {
            train.MoveNext();
            train.OnPathEnded += OnPathEnded;
        });
    }

    public void CheckCollisions()
    {
        while (_activePathes > 0 && !_isCrashed)
        {
            for (var i = 0; i < _map.Trains.Count; i++)
            {
                var train = _map.Trains[i];
                train.MoveNext();

            }

            if (_isToIttarate)
            {
                foreach (var mapPath in _map.Paths)
                {
                    Console.WriteLine($"{mapPath.Name()} - {mapPath.GetTrain()}");
                }

                Console.ReadLine();
            }
        }

        if (!_isCrashed)
            Console.WriteLine("Столкновений нет");
    }

    private void OnTrainCrash(string res)
    {
        Console.WriteLine(res);
        _isCrashed = true;
    }
}