﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace SitronicsTestTask
{
    public class Train
    {
        public int Id { get; }
        public event Action<Train> OnPathEnded;

        private Queue<IPath> _path;

        private int _currentPosition;
        private IPath CurrentPath { get; set; }
        private bool _isTrainStoped = false;


        public Queue<IPath> Path => _path;

        public string PathString
        {
            get
            {
                var res = "";
                if (_path is { Count: > 0 })
                    _path.ToList().ForEach(p => res += $"{p.Name()} -> ");
                return res;
            }
        }


        [JsonConstructor]
        public Train(int id, Queue<IPath> path)
        {
            Id = id;
            _path = path;
            _currentPosition = 0;
        }

        public Train(int id)
        {
            Id = id;
        }

        public Queue<IPath> SetPath()
        {
            _path = new Queue<IPath>();
            return _path;
        }


        public void MoveNext()
        {
            if (_path is { Count: > 0 } && (CurrentPath is null || _currentPosition++ >= CurrentPath.GetLength()))
            {
                var previousPath = CurrentPath;
                _path.Peek().AddTrain(this);
                CurrentPath = _path.Dequeue();
                previousPath?.RemoveTrain(this);
                _currentPosition = 0;
            }
            else if (_path == null || _path.Count == 0)
            {
                OnPathEnded?.Invoke(this);
            }
        }

        public override string ToString()
        {
            return $"Поезд - {Id}";
        }
    }
}