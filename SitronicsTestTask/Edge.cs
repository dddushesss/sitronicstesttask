﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SitronicsTestTask
{
    public class Edge : ITrainHandler, IPath
    {

        private Train _train;
        [JsonProperty] public int Cost { get; }
        [JsonProperty] public Node FirstNode { get; }
        [JsonProperty] public Node NextNode { get; }
        [JsonProperty] public int Id { get; }
        public event Action<string> OnTrainCrash;

       
        [JsonConstructor]

        public Edge(int cost, Node firstNode, Node nextNode, int id)
        {
            Cost = cost;
            FirstNode = firstNode;
            NextNode = nextNode;
            Id = id;
        }
        
        public Edge(int cost, int firstNode, int nextNode, IList<Node> nodes, int id)
        {
            Cost = cost;
            FirstNode = nodes[firstNode];
            NextNode = nodes[nextNode];
            Id = id;
        }


        public void AddTrain(Train train)
        {
            
            if (_train is not null)
            {
                OnTrainCrash?.Invoke($"Поезд № {_train.Id} поезд {train.Id} столкнулись на пути № {Id}");
            }

            _train = train;
        }

        public Train GetTrain()
        {
            return _train;
        }

        public void RemoveTrain(Train train)
        {
            _train = null;
        }


        public int GetLength()
        {
            return Cost;
        }

        public string Name()
        {
            return $"Путь № {Id}";
        }
        
    }
}