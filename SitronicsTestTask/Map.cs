﻿using System.Collections.Generic;
using System.Linq;

namespace SitronicsTestTask;

public class Map
{
    
    public List<Node> Stations { get; }

    public List<Edge> Pathes { get; }
    
    public List<Train> Trains { get; }

    public List<ITrainHandler> TrainHandlers
    {
        get
        {
            var res = new List<ITrainHandler>();
            Pathes.ForEach(path =>
            {
                res.Add(path);
            });
            Stations.ForEach(node =>
            {
                res.Add(node);
            });
            return res;
        }
    }

    public List<IPath> Paths
    {
        get
        {
            var res = new List<IPath>();
            Pathes.ForEach(path =>
            {
                res.Add(path);
            });
            Stations.ForEach(node =>
            {
                res.Add(node);
            });
            return res;
        }
    }

    public Map()
    {
        Pathes = new List<Edge>();
        Stations = new List<Node>();
        Trains = new List<Train>();
    }

    public Map(MapData data)
    {
        Pathes = new List<Edge>();
        Stations = new List<Node>();
        Trains = new List<Train>();
        data.Nodes.ForEach(node => Stations.Add(node));
        data.Edges.ForEach(edge => Pathes.Add(edge));
        data.Trains.ForEach(train =>
        {
            var path = new Queue<IPath>(train.Path.Select(p => Paths.Find(path => p.Name().Equals(path.Name()))));
            var t = new Train(train.Id, path);
            Trains.Add(t);
            
        });
    }
   

    public void AddStation(Node node)
    {
        Stations.Add(node);
    }

    public void AddEdge(Edge edge)
    {
        Pathes.Add(edge);
    }

    public void AddTrain(Train train)
    {
        Trains.Add(train);
    }


}