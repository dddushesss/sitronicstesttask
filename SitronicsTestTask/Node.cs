﻿using System;
using Newtonsoft.Json;

namespace SitronicsTestTask
{
    public class Node : ITrainHandler, IPath
    {
        [JsonProperty]
        public int Id { get; }
        private Train _train;
        public event Action<string> OnTrainCrash;

        public Node(int id)
        {
            Id = id;
           
        }

        public void AddTrain(Train train)
        {
            
            if (_train is not null)
            {
                OnTrainCrash?.Invoke($"Поезд № {_train.Id} поезд {train.Id} столкнулись на станции № {Id}");
            }

            _train = train;
        }

        public Train GetTrain()
        {
            return _train;
        }

        public void RemoveTrain(Train train)
        {
            _train = null;
        }

        public int GetLength()
        {
            return 1;
        }

        public string Name()
        {
            return $"Станция № {Id}";
        }

        
    }
}