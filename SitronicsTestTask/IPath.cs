﻿using System;

namespace SitronicsTestTask;

public interface IPath
{
    public int GetLength();
    public string Name();
    public void AddTrain(Train train);
    public void RemoveTrain(Train train);
    public Train GetTrain();
}