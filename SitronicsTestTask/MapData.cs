﻿using System.Collections.Generic;
using System.Linq;

namespace SitronicsTestTask;

public class MapData
{
    public List<Train> Trains { get; }
    public List<Node> Nodes { get; }
    public List<Edge> Edges { get; }

    public MapData()
    {
        Edges = new List<Edge>();
        Trains = new List<Train>();
        Nodes = new List<Node>();
    }
    
    public bool IsConnected(int firstNode, int secondNode)
    {
        return Edges.Any(path =>
            (path.FirstNode.Id == secondNode && path.NextNode.Id == firstNode) ||
            (path.FirstNode.Id == firstNode && path.NextNode.Id == secondNode));
    }

    public bool HasConnections(int nodeId)
    {
        return Edges.Any(path => path.FirstNode.Id == nodeId || path.NextNode.Id == nodeId);
    }

    public Node[] GetConnections(int nodeId)
    {
        var result = Edges.Where(path => path.FirstNode.Id == nodeId).Select(path => path.NextNode).ToList();
        result.AddRange(Edges.Where(path => path.NextNode.Id == nodeId).Select(path => path.FirstNode));
        return result.ToArray();
    }
    
    public Edge GetConnection(int firstId, int nextId)
    {
        return Edges.First(path =>
            (path.FirstNode.Id == firstId && path.NextNode.Id == nextId) ||
            (path.FirstNode.Id == nextId && path.NextNode.Id == firstId));
    }
}