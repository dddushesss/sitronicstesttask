﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace SitronicsTestTask;

public class InputParser
{
    private string _filepath;
    private bool _isCreatingData;
    private bool _isToItterate = false;

    public InputParser(string filePath, bool isCreatingData, bool isToItterate)
    {
        _filepath = filePath;
        _isCreatingData = isCreatingData;
        _isToItterate = isToItterate;
    }

    public void HandleInput()
    {
        if (_isCreatingData)
        {
            CreateData();
        }
        else
        {
            ReadData();
        }
    }

    private void ReadData()
    {
        MapData mapData = null;
        try
        {
            using StreamReader reader = new StreamReader(_filepath);
            using JsonReader jreader = new JsonTextReader(reader);
            mapData = JsonConvert.DeserializeObject<MapData>(File.ReadAllText(_filepath), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore,
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return;
        }
       

       

        
        if (mapData is null)
        {
            Console.WriteLine("Ошибка десериализации");
            return;
        }

        Console.Clear();
        var map = new Map(mapData);
        Console.Write("Станции: ");
        map.Stations.ForEach(station => Console.Write($"{station.Id}\t"));
        Console.Write("\nПоезда: ");
        map.Trains.ForEach(train => Console.Write($"{train.Id}\t"));
        Console.Write("\nПути:");
        map.Pathes.ForEach(path =>
            Console.Write($"{path.FirstNode.Id} <-({path.Cost})-> {path.NextNode.Id};\t"));
        Console.WriteLine("\nМаршруты: ");
        map.Trains.ForEach(train => Console.WriteLine($"Поезд № {train.Id}: {train.PathString}"));
        MapManager manager = new MapManager(map);
        manager.Init(_isToItterate);
        manager.CheckCollisions();

    }

    
    private void CreateData()
    {
        var map = new MapData();
        Console.WriteLine(
            $"1 - добавть станцию, 2 - добавить путь, 3 - добавть поезд, 4 - добавить маршрут, 5 - закончить и записать в файл {_filepath}");
        var input = Console.ReadLine();


        while (input != null && !input.Equals("5"))
        {
            switch (input.Trim())
            {
                case "1":
                    var node = new Node(map.Nodes.Count);
                    map.Nodes.Add(node);
                    break;
                case "2":
                    AddPath(map);
                    break;
                case "3":
                    map.Trains.Add(new Train(map.Trains.Count));
                    break;
                case "4":
                    AddTrack(map);
                    break;
                default:
                    break;
            }

            Console.Clear();
            Console.Write("Станции: ");
            map.Nodes.ForEach(station => Console.Write($"{station.Id}\t"));
            Console.Write("\nПоезда: ");
            map.Trains.ForEach(train => Console.Write($"{train.Id}\t"));
            Console.Write("\nПути:");
            map.Edges.ForEach(path =>
                Console.Write($"{path.FirstNode.Id} <-({path.Cost})-> {path.NextNode.Id};\t"));
            Console.WriteLine("\nМаршруты: ");
            map.Trains.ForEach(train => Console.WriteLine($"Поезд № {train.Id}: {train.PathString}"));
            Console.WriteLine(
                $"\n1 - добавть станцию, 2 - добавить путь, 3 - добавть поезд, 4 - добавить маршрут, 5 - закончить и записать в файл {_filepath}");
            input = Console.ReadLine();
        }

        var serializer = new JsonSerializer();


        using StreamWriter sw = new StreamWriter($"{_filepath}");
        using JsonWriter writer = new JsonTextWriter(sw);
        serializer.NullValueHandling = NullValueHandling.Ignore;
        serializer.TypeNameHandling = TypeNameHandling.Auto;
        serializer.Formatting = Formatting.Indented;
        serializer.Serialize(writer, map);
    }
    
    private void AddPath(MapData map)
    {
        Console.WriteLine("Введите id первой станции");
        if (!int.TryParse(Console.ReadLine(), out var first) || first > map.Nodes.Count - 1)
        {
            Console.WriteLine("Ошибка");
            return;
        }

        Console.WriteLine("Введите id второй станции");
        if (!int.TryParse(Console.ReadLine(), out var second) || second > map.Nodes.Count - 1)
        {
            Console.WriteLine("Ошибка");
            return;
        }

        Console.WriteLine("Введите расстояние");
        if (!int.TryParse(Console.ReadLine(), out var cost))
        {
            Console.WriteLine("Ошибка");
            return;
        }

        var edge = new Edge(cost, first, second, map.Nodes, map.Edges.Count);
        map.Edges.Add(edge);
    }

    private void AddTrack(MapData map)
    {
        if (map.Edges.Count == 0)
        {
            Console.WriteLine("Ошибка: нет заданых путей");
        }

        Console.WriteLine("Введите id поезда");
        if (!int.TryParse(Console.ReadLine(), out var trainId) || trainId > map.Trains.Count - 1)
        {
            Console.WriteLine("Ошибка");
            return;
        }

        var path = map.Trains[trainId].SetPath();


        Console.WriteLine("Введите id начальной станции");

        var inputPath = Console.ReadLine();
        if (!int.TryParse(inputPath, out var stationId) || stationId > map.Nodes.Count - 1)
        {
            Console.WriteLine("Ошибка");
            return;
        }

        if (!map.HasConnections(stationId))
        {
            Console.WriteLine("Ошибка - нет путей к этой станции");
            return;
        }

        path.Enqueue(map.Nodes[stationId]);

        while (inputPath != null && !inputPath.Equals("end"))
        {
            Console.WriteLine("Выберете следующую станцию, end - для завершения:");
            var previousStation = stationId;

            map.GetConnections(previousStation).ToList()
                .ForEach(p => Console.Write($"{p.Id}\t"));

            inputPath = Console.ReadLine();

            if (inputPath is "end")
            {
                continue;
            }

            if (!int.TryParse(inputPath, out stationId) || stationId > map.Edges.Count - 1 ||
                map.GetConnection(stationId, previousStation) is null)
            {
                Console.WriteLine("Ошибка");
                continue;
            }

            path.Enqueue(map.GetConnection(stationId, previousStation));
            path.Enqueue(map.Nodes[stationId]);
        }
    }

}